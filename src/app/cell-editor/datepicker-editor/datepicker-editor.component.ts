import {AfterViewInit, Component, ViewChild, ViewContainerRef} from '@angular/core';
import {ICellEditorAngularComp} from 'ag-grid-angular';
import * as moment from 'moment';

@Component({
  selector: 'app-date-picker-editor',
  templateUrl: './datepicker-editor.component.html',
  styleUrls: ['datepicker-editor.component.scss'],
})
export class DatepickerEditorComponent implements ICellEditorAngularComp, AfterViewInit {
  private params: any;
  public dateValue;

  @ViewChild('container', {read: ViewContainerRef}) public container;

  ngAfterViewInit() {
    this.container.element.nativeElement.focus();
  }

  agInit(params: any): void {
    this.params = params;
    this.dateValue = this.params.value ? moment.unix(this.params.value).toDate() : new Date();
  }

  getValue(): any {
    return Math.round(this.dateValue.getTime() / 1000);
  }

  onClick(event) {
    if (event.srcElement.innerText >= 1 && event.srcElement.innerText <= 31) {
      this.params.stopEditing();
    }
  }

  isPopup(): boolean {
    return true;
  }
}
