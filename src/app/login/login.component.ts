import {Component} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {FormControl, Validators} from '@angular/forms';
import {PortService} from '../services/port.service';
import {PortListModel} from '../models/port-list.model';
import {MatDialog} from '@angular/material';
import {MessageDialogComponent} from '../dialog/message-dialog/message-dialog.component';
import {Router} from '@angular/router';
import {SupportService} from '../services/support.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public isLoginProcessing = false;
  public hide = true;

  public ctrlEmail = new FormControl('', [Validators.required, Validators.email]);
  public ctrlPassword = new FormControl('', [Validators.required]);
  public ctrlRemember = new FormControl();

  private portList = new PortListModel;

  constructor(private router: Router, private auth: AuthService, private port: PortService, private dialog: MatDialog, private support: SupportService) {
    if (localStorage.getItem('auth_remember') === 'true') {
      this.ctrlEmail.setValue(localStorage.getItem('auth_email'));
      this.ctrlPassword.setValue(localStorage.getItem('auth_password'));
      this.ctrlRemember.setValue(localStorage.getItem('auth_remember'));
    }
  }

  onClickLogin(): void {
    this.isLoginProcessing = true;
    this.auth.login(this.ctrlEmail.value, this.ctrlPassword.value).subscribe(res => {
      console.log(res);
      if (res.access_token) {
        localStorage.setItem('auth_email', this.ctrlEmail.value);
        localStorage.setItem('auth_password', this.ctrlPassword.value);
        localStorage.setItem('auth_remember', this.ctrlRemember.value);
        localStorage.setItem('access_token', res.access_token);
        this.onLoadPortList();
      } else {
        this.isLoginProcessing = false;
      }
    }, () => {
      this.isLoginProcessing = false;
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'Wrong Email or Password has been entered.'}
      });
    });
  }

  ////////////////////////////// Load Data //////////////////////////////

  private onLoadPortList(): void {
    this.port.getPortList().then(obj => {
      console.log(obj);
      Object.keys(this.portList).forEach(key => {
        obj.forEach(elem => {
          if (key === elem.port.slice(0, 1).toLowerCase()) {
            this.portList[key].push(elem);
          }
        });
      });
      localStorage.setItem('json_port', JSON.stringify(this.portList));
      this.onLoadRoles();
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'json_port ' + JSON.parse(error._body).message}
      });
    });
  }

  private onLoadRoles(): void {
    this.support.getRole().then(res => {
      localStorage.setItem('json_roles', JSON.stringify(res.data));
      this.onLoadCountry();
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'json_roles ' + JSON.parse(error._body).message}
      });
    });
  }

  private onLoadCountry(): void {
    this.support.getCountry().subscribe(res => {
      localStorage.setItem('json_country', JSON.stringify(res.json()));
      this.onLoadCompanyType();
    });
  }

  private onLoadCompanyType(): void {
    this.support.getCompanyType().then(res => {
      localStorage.setItem('json_company_type', JSON.stringify(res.data));
      this.onLoadExpenseCategory();
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'json_company_type ' + JSON.parse(error._body).message}
      });
    });
  }

  private onLoadExpenseCategory(): void {
    this.support.getExpenseCategory().then(res => {
      localStorage.setItem('json_expense_category', JSON.stringify(res.data));
      this.router.navigate(['/']);
      this.isLoginProcessing = false;
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'json_company_type ' + JSON.parse(error._body).message}
      });
    });
  }
}
