import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-text-filter',
  templateUrl: 'text-filter.component.html',
  styleUrls: ['./text-filter.component.scss'],
})
export class TextFilterComponent {
  @Input() placeholder = '';

  @Input() filterGroup = '';

  @Output() filterChanged = new EventEmitter();

  onInputSearchName(event): void {
    this.filterChanged.emit({group: this.filterGroup, value: event.srcElement.value});
  }
}
