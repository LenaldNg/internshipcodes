import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material';
import {DateFormat} from '../../shared/date-format';
import {MomentDateAdapter} from '@angular/material-moment-adapter';

@Component({
  selector: 'app-date-form-field',
  templateUrl: 'date-form-field.component.html',
  styleUrls: ['./date-form-field.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: DateFormat},
  ],
})
export class DateFormFieldComponent {

  @Input() floatLabel = 'always'; // always / auto / never

  @Input() fieldRequired = false; // false / true

  @Input() placeholder = '';

  @Input() fieldFormControl: FormControl;
}
