import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {ActionButtonComponent} from './action-button.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
  ],
  exports: [
    ActionButtonComponent
  ],
  declarations: [
    ActionButtonComponent
  ],
})
export class ActionButtonModule {
}
