import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-action-button',
  templateUrl: 'action-button.component.html',
  styleUrls: ['./action-button.component.scss'],
})
export class ActionButtonComponent {
  @Input() stackAddIcon = false;

  @Input() btnText = 'Button';

  @Input() cssStyle = 'btn-black';

  @Input() fontAwesome = '';

  @Input() disabled = false;

  @Output() clickEvent = new EventEmitter();

  onClickEvent(): void {
    this.clickEvent.emit();
  }
}
