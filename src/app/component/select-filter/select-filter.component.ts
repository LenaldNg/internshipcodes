import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-select-filter',
  templateUrl: 'select-filter.component.html',
  styleUrls: ['./select-filter.component.scss'],
})
export class SelectFilterComponent {
  @Input() placeholder = '';

  @Input() filterList = [];

  @Input() filterGroup = '';

  @Input() itemDisplay = 'value';

  @Input() itemValue = 'id';

  @Output() filterChanged = new EventEmitter();

  public filterItem = '';

  onFilterList(event): void {
    console.log(event);
    this.filterChanged.emit({group: this.filterGroup, value: event.value});
  }
}
