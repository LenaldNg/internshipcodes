import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {SelectFilterComponent} from './select-filter.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
  ],
  exports: [
    SelectFilterComponent
  ],
  declarations: [
    SelectFilterComponent
  ],
})
export class SelectFilterModule {
}
