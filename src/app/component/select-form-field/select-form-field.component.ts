import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-select-form-field',
  templateUrl: 'select-form-field.component.html',
  styleUrls: ['./select-form-field.component.scss'],
})
export class SelectFormFieldComponent {

  @Input() floatLabel = 'always'; // always / auto / never

  @Input() fieldRequired = false; // false / true

  @Input() placeholder = '';

  @Input() fieldFormControl: FormControl;

  @Input() arrayList = [];

  @Input() itemDisplay = '';

  @Input() itemDisplaySmall = '';

  @Input() itemValue = '';

  @Output() selectionChange = new EventEmitter();

  onSelectionChange(event): void {
    console.log(event);
    this.selectionChange.emit(event.value);
  }
}
