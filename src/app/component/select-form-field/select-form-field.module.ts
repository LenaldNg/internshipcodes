import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {SelectFormFieldComponent} from './select-form-field.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
  ],
  exports: [
    SelectFormFieldComponent
  ],
  declarations: [
    SelectFormFieldComponent
  ],
})
export class SelectFormFieldModule {
}
