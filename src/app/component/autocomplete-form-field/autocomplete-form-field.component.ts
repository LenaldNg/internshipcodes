import {Component, Input, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-autocomplete-form-field',
  templateUrl: 'autocomplete-form-field.component.html',
  styleUrls: ['./autocomplete-form-field.component.scss'],
})
export class AutocompleteFormFieldComponent implements OnInit {

  @Input() floatLabel = 'always'; // always / auto / never

  @Input() fieldRequired = false; // false / true

  @Input() placeholder = '';

  @Input() fieldFormControl: FormControl;

  @Input() arrayList = [];

  @Input() itemDisplay = '';

  filteredOptions: Observable<string[]>;

  ngOnInit() {
    this.filteredOptions = this.fieldFormControl.valueChanges.pipe(
      startWith(''), map(val => val ? this.filter(val) : this.arrayList.slice())
    );
  }

  filter(val: string): string[] {
    return this.arrayList.filter(option => option[this.itemDisplay].toLowerCase().includes(val.toLowerCase()));
  }
}
