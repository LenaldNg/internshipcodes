import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {AutocompleteFormFieldComponent} from './autocomplete-form-field.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
  ],
  exports: [
    AutocompleteFormFieldComponent
  ],
  declarations: [
    AutocompleteFormFieldComponent
  ],
})
export class AutocompleteFormFieldModule {
}
