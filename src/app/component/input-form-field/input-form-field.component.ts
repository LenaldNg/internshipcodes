import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-input-form-field',
  templateUrl: 'input-form-field.component.html',
  styleUrls: ['./input-form-field.component.scss'],
})
export class InputFormFieldComponent {

  @Input() floatLabel = 'always'; // always / auto / never

  @Input() hintLabel = '';

  @Input() type = 'text'; // text / number / password

  @Input() fieldRequired = false; // false / true

  @Input() placeholder = '';

  @Input() fieldFormControl: FormControl;

  @Input() unit = '';

  @Input() characterLength = '';

  @Input() prefix = '';

  @Input() textAlign = '';

  public hide = true;
}
