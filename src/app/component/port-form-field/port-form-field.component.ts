import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-port-form-field',
  templateUrl: 'port-form-field.component.html',
  styleUrls: ['./port-form-field.component.scss'],
})
export class PortFormFieldComponent implements OnInit {

  @Input() floatLabel = 'always'; // always / auto / never

  @Input() fieldRequired = false; // false / true

  @Input() placeholder = '';

  @Input() fieldFormControl: FormControl;

  @Input() isPortSelected: boolean;

  @Output() valueSelected = new EventEmitter();

  filteredOptions: Observable<string[]>;

  public portList = [];

  ngOnInit() {
    this.filteredOptions = this.fieldFormControl.valueChanges.pipe(
      startWith({} as any),
      map(port => port && typeof port === 'object' ? port.port : port),
      map(val => val ? this.filter(val) : this.portList.slice())
    );
  }

  filter(val: string): string[] {
    return val ? this.portList.filter(option => option['port'].toLowerCase().includes(val.toLowerCase())) : this.portList;
  }

  onInputPortName(event): void {
    this.portList = [];
    this.valueSelected.emit(false);
    if (event.srcElement.value.length > 2) {
      this.portList = JSON.parse(localStorage.getItem('json_port'))[event.srcElement.value[0].toLowerCase()];
    }
  }

  onOptionSelected(event): void {
    this.valueSelected.emit(true);
  }
}
