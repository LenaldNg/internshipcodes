import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {PortFormFieldComponent} from './port-form-field.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
  ],
  exports: [
    PortFormFieldComponent
  ],
  declarations: [
    PortFormFieldComponent
  ],
})
export class PortFormFieldModule {
}
