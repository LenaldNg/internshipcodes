import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-company-dialog',
  templateUrl: 'company-dialog.component.html',
  styleUrls: ['./company-dialog.component.scss'],
})
export class CompanyDialogComponent implements OnInit {
  public companyForm: FormGroup;

  public color;

  public countryList = [];
  public companyTypeList = [];

  public isPortSelected = false;

  constructor(public dialogRef: MatDialogRef<CompanyDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, public fb: FormBuilder, private api: ApiService) {
    this.isPortSelected = !!data.port;

    this.companyForm = fb.group({
      name: new FormControl(data.name, [Validators.required]),
      type_id: new FormControl(data.type_id, [Validators.required]),
      registration_number: new FormControl(data.registration_number, [Validators.required]),
      country_code: new FormControl(data.country_code, [Validators.required]),
      port: new FormControl(data.port, [Validators.required]),
      address_1: new FormControl(data.address_1, [Validators.required]),
      postal_code: new FormControl(data.postal_code, [Validators.required]),
      phone_number: new FormControl(data.phone_number),
      da_fee: new FormControl(data.da_fee, [Validators.required]),
      da_balance: new FormControl(data.da_balance, [Validators.required]),
      color_code: new FormControl(data.color_code ? '#' + data.color_code : '#25af5f'),
    });
  }

  ngOnInit() {
    this.companyTypeList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_company_type')), 'description');
    this.countryList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_country')), 'name');
  }

  ///////////////// Click Event /////////////////

  onClickClose(): void {
    this.dialogRef.close();
  }

  onClickCreate(): void {
    const form = this.companyForm.getRawValue();
    form.color_code = this.companyForm.value.color_code.replace('#', '');
    this.api.createCompany(form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px',
        data: {status: 'success', message: 'Company ' + this.companyForm.value.name.toUpperCase() + ' is created.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onClickUpdate(): void {
    const form = this.companyForm.getRawValue();
    form.color_code = this.companyForm.value.color_code.replace('#', '');
    this.api.updateCompany(this.data.id, form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px',
        data: {status: 'success', message: 'Company ' + this.companyForm.value.name.toUpperCase() + '\'s data is updated'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onValueSelected(value): void {
    this.isPortSelected = value;
  }
}
