import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import * as moment from 'moment';
import {SupportService} from '../../services/support.service';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-vessel-dialog',
  templateUrl: 'vessel-dialog.component.html',
  styleUrls: ['./vessel-dialog.component.scss'],
})
export class VesselDialogComponent implements OnInit {
  public vesselForm: FormGroup;

  public countryList = [];
  public companyList = [];
  public coatingList = [];
  public vesselClassList = [];
  public vesselTypeList = [];

  constructor(public dialogRef: MatDialogRef<VesselDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, public fb: FormBuilder,
              private support: SupportService, private api: ApiService) {
    this.vesselForm = fb.group({
      id: new FormControl(this.data.id),
      company_id: new FormControl(parseInt(this.data.company_id, 10), [Validators.required]),
      name: new FormControl(this.data.name, [Validators.required]),
      dwt: new FormControl(this.data.dwt, [Validators.required, Validators.min(0)]),
      loa: new FormControl(this.data.loa, [Validators.required, Validators.min(0)]),
      beam: new FormControl(this.data.beam, [Validators.required, Validators.min(0)]),
      draft: new FormControl(this.data.draft, [Validators.required, Validators.min(0)]),
      seg: new FormControl(this.data.seg, [Validators.required, Validators.min(0)]),
      coating: new FormControl(this.data.coating, [Validators.required]),
      capacity: new FormControl(this.data.capacity, [Validators.required, Validators.min(0)]),
      built: new FormControl({value: CommonCode.unixToDate(this.data.built, ''), disabled: true}, [Validators.required]),
      vessel_class: new FormControl(this.data.vessel_class, [Validators.required]),
      flag: new FormControl(this.data.flag, [Validators.required]),
      type: new FormControl(this.data.type, [Validators.required]),
      joined_at: new FormControl({value: CommonCode.unixToDate(this.data.joined_at, ''), disabled: true}, [Validators.required]),
      imo: new FormControl(this.data.imo, [Validators.required, Validators.min(1000000), Validators.max(9999999)]),
      mmsi: new FormControl(this.data.mmsi, [Validators.required, Validators.min(100000000), Validators.max(999999999)]),
      call_sign: new FormControl(this.data.call_sign, [Validators.required]),
      grt: new FormControl(this.data.grt, [Validators.required, Validators.min(0)]),
    });
  }

  ngOnInit() {
    this.coatingList = [{id: 'EPOXY', value: 'EPOXY'}, {id: 'MARINE LINE', value: 'MARINE LINE'}, {id: 'PHENOLIC EPOXY', value: 'PHENOLIC EPOXY'},
      {id: 'SUS304', value: 'SUS304'}, {id: 'SUS316', value: 'SUS316'}, {id: 'SUS316L', value: 'SUS316L'}, {id: 'ZINC', value: 'ZINC'}];
    this.vesselClassList = [{id: 'NK', value: 'NK'}];
    this.vesselTypeList = [{id: 'CHEMICAL', value: 'CHEMICAL'}, {id: 'LPG', value: 'LPG'}];
    this.countryList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_country')), 'name');

    this.support.getCompanyList().then(res => {
      res.data.forEach(item => this.companyList.push(item));
      this.companyList = CommonCode.filterAndSortList(this.companyList, 'name');
    });
  }

  ///////////////// Click Event /////////////////

  onClickClose(): void {
    this.dialogRef.close();
  }

  onClickCreate(): void {
    const form = this.vesselForm.getRawValue();
    form.built = CommonCode.dateToUnit(form.built);
    this.api.createVessel(form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: 'Vessel ' + form.name.toUpperCase() + ' is created.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onClickUpdate(): void {
    const form = this.vesselForm.getRawValue();
    form.built = CommonCode.dateToUnit(form.built);
    this.api.updateVessel(form.id, form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: 'Vessel ' + form.name.toUpperCase() + ' is updated.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }
}
