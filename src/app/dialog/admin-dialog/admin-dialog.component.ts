import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import {ApiService} from '../../services/api.service';
import {SupportService} from '../../services/support.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-admin-dialog',
  templateUrl: 'admin-dialog.component.html',
  styleUrls: ['./admin-dialog.component.scss'],
})
export class AdminDialogComponent implements OnInit {
  public userForm: FormGroup;

  public countryList = [];
  public companyList = [];
  public roleList = [];

  constructor(public dialogRef: MatDialogRef<AdminDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, public fb: FormBuilder,
              public support: SupportService, public api: ApiService) {
    console.log(this.data);
    this.userForm = fb.group({
      first_name: new FormControl(data.first_name, [Validators.required]),
      last_name: new FormControl(data.last_name, [Validators.required]),
      email: new FormControl(data.email, [Validators.required, Validators.email]),
      phone_country_code: new FormControl(data.phone_country_code, [Validators.required]),
      phone_number: new FormControl(data.phone_number, [Validators.required, Validators.min(0)]),
      company_id: new FormControl(data.company_id, [Validators.required]),
      role_id: new FormControl(data.role_id, [Validators.required]),
    });
  }

  ngOnInit() {
    this.countryList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_country')), 'dial_code');
    this.roleList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_roles')), 'description');

    this.support.getCompanyList().then(res => {
      res.data.forEach(item => this.companyList.push(item));
      this.companyList = CommonCode.filterAndSortList(this.companyList, 'name');
    });
  }

  ///////////////// Click Event /////////////////

  onClickClose(): void {
    this.dialogRef.close();
  }

  onClickCreate(): void {
    const form = this.userForm.getRawValue();
    form.password = 'baycrest000';
    this.api.createUserAdmin(form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px',
        data: {status: 'success', message: 'User ' + this.userForm.value.first_name.toUpperCase() + ' is created.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onClickUpdate(): void {
    this.api.updateUserAdmin(this.data.id, this.userForm.getRawValue()).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px',
        data: {status: 'success', message: 'User ' + this.userForm.value.first_name.toUpperCase() + '\'s data is updated'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }
}
