import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-expense-list-dialog',
  templateUrl: 'expense-list-dialog.component.html',
  styleUrls: ['./expense-list-dialog.component.scss'],
})
export class ExpenseListDialogComponent implements OnInit {
  public expenseForm: FormGroup;
  public categoryList = [];

  constructor(public dialogRef: MatDialogRef<ExpenseListDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, public fb: FormBuilder,
              private api: ApiService) {
    this.expenseForm = fb.group({
      id: new FormControl({value: data.data.id ? data.data.id[0] : '', disabled: !!data.data.id}, [Validators.required]),
      description: new FormControl(data.data.description, [Validators.required]),
    });
  }

  ngOnInit() {
    this.categoryList = JSON.parse(localStorage.getItem('json_expense_category'));
  }

  ///////////////// Click Event /////////////////

  onClickClose(): void {
    this.dialogRef.close();
  }

  onClickCreate(): void {
    let list = [];
    this.data.expenseList.forEach(res => {
      if (res.id.slice(0, 1) === this.expenseForm.controls['id'].value) {
        list.push(res);
      }
    });
    list = CommonCode.sortList(list, 'id');

    this.expenseForm.controls['id'].setValue(this.expenseForm.value.id + CommonCode.pad((Number(list[list.length - 1] ? list[list.length - 1].id.slice(1, 5) : 0) + 1), 4));

    this.api.createExpenseList(this.expenseForm.getRawValue()).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: 'New Port Expense List is created.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onClickUpdate(): void {
    this.expenseForm.controls['id'].setValue(this.data.data.id);

    this.api.updateExpenseList(this.data.data.id, this.expenseForm.getRawValue()).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: this.expenseForm.value.id + ' has been updated.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }
}
