import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {MessageDialogComponent} from '../message-dialog/message-dialog.component';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-nomination-dialog',
  templateUrl: 'nomination-dialog.component.html',
  styleUrls: ['./nomination-dialog.component.scss'],
})
export class NominationDialogComponent implements OnInit {
  public epdaForm: FormGroup;
  public ownerList = [];
  public agentList = [];
  public picList = [];
  public vesselList = [];
  public checkBoxList = {bunker: false, cargo: false, crew: false, other: false};

  public isPortSelected = false;

  constructor(public dialogRef: MatDialogRef<NominationDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: any, public dialog: MatDialog, public fb: FormBuilder,
              private api: ApiService) {
    this.epdaForm = fb.group({
      date: new FormControl(''),
      owner_company_id: new FormControl('', [Validators.required]),
      pic_user_id: new FormControl('', [Validators.required]),
      vessel_id: new FormControl('', [Validators.required]),
      voyage_number: new FormControl('', [Validators.required, Validators.min(0)]),
      agent_company_id: new FormControl('', [Validators.required]),
      port: new FormControl('', [Validators.required]),
      country: new FormControl(''),
      date_arrival: new FormControl({value: new Date(), disabled: true}, [Validators.required]),
      date_departure: new FormControl({value: new Date(), disabled: true}, [Validators.required]),
      purposes: new FormControl(''),
      status: new FormControl('created', [Validators.required]),
    });
  }

  ngOnInit() {
    this.data.companyList.forEach(res => {
      if (res.type_id === 5) {
        this.ownerList.push(res);
      } else if (res.type_id === 3) {
        this.agentList.push(res);
      }
    });
  }

  ///////////////// Click Event /////////////////

  onClickClose(): void {
    this.dialogRef.close();
  }

  onClickCreate(): void {
    console.log(this.epdaForm.getRawValue());
    const purposes = [];
    for (const property in this.checkBoxList) {
      if (this.checkBoxList.hasOwnProperty(property)) {
        if (this.checkBoxList[property]) {
          purposes.push(property);
        }
      }
    }

    this.epdaForm.controls['date'].setValue(Math.round(Date.now() / 1000));
    this.epdaForm.controls['purposes'].setValue(purposes);

    const form = this.epdaForm.getRawValue();
    form.date_arrival = CommonCode.dateToUnit(this.epdaForm.getRawValue().date_arrival);
    form.date_departure = CommonCode.dateToUnit(this.epdaForm.getRawValue().date_departure);
    console.log(form);
    this.api.createNominationList(form).then(() => {
      this.dialogRef.close();
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: 'New Nomination List is created.'}
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  ///////////////// Data Emit /////////////////

  onOwnerSelectionChange(value): void {
    this.picList = [];
    this.vesselList = [];
    this.data.userList.forEach(res => {
      if (res.company_id === value) {
        this.picList.push(res);
      }
    });

    this.data.vesselList.forEach(res => {
      if (res.company_id === value) {
        this.vesselList.push(res);
      }
    });
  }

  onValueSelected(value): void {
    const country = JSON.parse(localStorage.getItem('json_port'))[this.epdaForm.value.port[0].toLowerCase()].find(res => res.port === this.epdaForm.value.port);
    this.epdaForm.controls['country'].setValue(country ? country.country : '');
    this.isPortSelected = value;
  }
}
