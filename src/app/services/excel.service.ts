import * as moment from 'moment';
import * as FileSaver from 'file-saver';

export class ExcelService {

  createExcel(workbook, companyName, workSheetName) {
    workbook.creator = companyName;
    workbook.lastModifiedBy = companyName;
    workbook.created = new Date();
    workbook.modified = new Date();
    workbook.lastPrinted = new Date();

    const fileName = '[' + workSheetName + '] ' + companyName + '_' + moment.unix(Date.now() / 1000).format('DDMMMYYYY') + '.xlsx';

    workbook.xlsx.writeBuffer().then(data => {
      const blob = new Blob([data], {type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'});
      FileSaver.saveAs(blob, fileName);
    });
  }
}
