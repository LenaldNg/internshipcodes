import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PortService {

  constructor(private httpClient: HttpClient) {

  }

  public getPortList() {
    return this.httpClient.get('https://s3-ap-southeast-1.amazonaws.com/mtec.resources/ports.json')
      .toPromise().then(response => response).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
