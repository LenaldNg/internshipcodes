import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';

@Injectable()
export class HttpClient {
  private JWT;

  constructor(private http: Http) {
  }

  private getJWT() {
    this.JWT = localStorage.getItem('access_token');
    return this.JWT;
  }

  createAuthorizationHeader(headers: Headers) {
    headers.set('Authorization', 'Bearer ' + this.getJWT());
    return headers;
  }

  get(url) {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.get(url, {headers});
  }

  post(url, data, option = {headers: null}) {
    if (!(option.headers)) {
      option.headers = new Headers();
    }
    option.headers = this.createAuthorizationHeader(option.headers);

    return this.http.post(url, data, option);
  }

  put(url, data) {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.put(url, this.getData(data), {headers});
  }

  delete(url) {
    const headers = new Headers();
    this.createAuthorizationHeader(headers);
    return this.http.delete(url, {headers});
  }

  getData(data) {
    Object.keys(data).forEach(key => {
      if (key === 'attributes' || key === '_attributes') {
        delete(data[key]);
      }
    });
    return data;
  }
}
