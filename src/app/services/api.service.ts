import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from './httpclient.class';

const API_URL = environment.API_URL;

@Injectable()
export class ApiService {

  constructor(private http: HttpClient) {
  }

  //////////////////// User Admin ////////////////////

  public getUserAdmin() {
    return this.http.get(API_URL + '/users').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public createUserAdmin(body) {
    return this.http.post(API_URL + '/users', body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public updateUserAdmin(id, body) {
    return this.http.put(API_URL + '/users/' + id, body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public deleteUserAdmin(id) {
    return this.http.delete(API_URL + '/users/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  //////////////////// Company ////////////////////

  public getCompany() {
    return this.http.get(API_URL + '/companies').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public createCompany(body) {
    return this.http.post(API_URL + '/companies', body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public updateCompany(id, body) {
    return this.http.put(API_URL + '/companies/' + id, body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public deleteCompany(id) {
    return this.http.delete(API_URL + '/companies/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  //////////////////// Vessel ////////////////////

  public getVesselList() {
    return this.http.get(API_URL + '/vessels').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public createVessel(body) {
    return this.http.post(API_URL + '/vessels', body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public updateVessel(id, body) {
    return this.http.put(API_URL + '/vessels/' + id, body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public deleteVessel(id) {
    return this.http.delete(API_URL + '/vessels/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  //////////////////// Expense List ////////////////////

  public getExpenseList() {
    return this.http.get(API_URL + '/expenses').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public createExpenseList(body) {
    return this.http.post(API_URL + '/expenses', body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public updateExpenseList(id, body) {
    return this.http.put(API_URL + '/expenses/' + id, body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public deleteExpenseList(id) {
    return this.http.delete(API_URL + '/expenses/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  //////////////////// Nomination List ////////////////////

  public getNominationList() {
    return this.http.get(API_URL + '/nominations').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public getNominationItem(id) {
    return this.http.get(API_URL + '/nominations/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public createNominationList(body) {
    return this.http.post(API_URL + '/nominations', body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public updateNominationList(id, body) {
    return this.http.put(API_URL + '/nominations/' + id, body).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public deleteNominationList(id) {
    return this.http.delete(API_URL + '/nominations/' + id).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
