import {Injectable} from '@angular/core';
import {HttpClient} from './httpclient.class';

@Injectable()
export class ShipService {

  constructor(private http: HttpClient) {
  }

  getAIS(data) {
    return this.http.post('http://206.189.35.91/api/vessels', data).toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
