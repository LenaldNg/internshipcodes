import {Injectable} from '@angular/core';
import {HttpClient} from './httpclient.class';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

const API_URL = environment.API_URL;

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {

  }

  public login(userId, password) {
    return this.http.post(API_URL + '/auth/login', {email: userId, password: password}).pipe(map(res => res.json()));
  }

  public getMe() {
    return this.http.get(API_URL + '/auth/me').pipe(map(res => res.json()));
  }

  updateUser(body) {
    return this.http.post(API_URL + '/auth/me', body)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  updatePassword(password) {
    return this.http.post(API_URL + '/auth/me', password)
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
