import {Injectable} from '@angular/core';
import {HttpClient} from './httpclient.class';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/index';

const API_URL = environment.API_URL;

@Injectable()
export class SupportService {

  constructor(private http: HttpClient) {

  }

  public getRole() {
    return this.http.get(API_URL + '/meta/roles').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public getCompanyList() {
    return this.http.get(API_URL + '/meta/companies').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public getCompanyType() {
    return this.http.get(API_URL + '/meta/company_types').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  public getCountry(): Observable<any> {
    return this.http.get('/assets/json/country.json');
  }

  public getExpenseCategory() {
    return this.http.get(API_URL + '/meta/expense_types').toPromise()
      .then(response => response.json()).catch(this.handleError);
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
