import {Component} from '@angular/core';
import {Location} from '@angular/common';

@Component({
  selector: 'app-layout-page',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent {
  private location: Location;
  public username = 'user';

  constructor(location: Location) {
    this.location = location;

    this.username = localStorage.getItem('user_first_name') + ' ' + localStorage.getItem('user_last_name');
  }

  getCurrentDirectory(): string {
    let titles = this.location.prepareExternalUrl(this.location.path());
    titles = titles.split('/')[1];
    titles = titles.replace('-', ' ');
    return titles === 'admin' ? 'User Admin' : titles === 'vessel' ? 'Fleet Management' : titles === 'expense-list' ? 'Port Expense List' : titles;
  }

  getDirectory(): string {
    let titles = this.location.prepareExternalUrl(this.location.path());
    titles = titles.split('/')[2];
    return titles ? titles.includes('nomination') ? 'NOMINATION LIST' : titles : '';
  }

  onClickLogout(): void {
    Object.keys(localStorage).forEach(key => {
      if (!(key === 'auth_email' || key === 'auth_password' || key === 'auth_remember')) {
        localStorage.removeItem(key);
      }
    });
  }
}
