import {Component} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {MessageDialogComponent} from '../../dialog/message-dialog/message-dialog.component';
import {MatDialog} from '@angular/material';
import {ExpenseListDialogComponent} from '../../dialog/expense-list-dialog/expense-list-dialog.component';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-expense-list',
  templateUrl: 'expense-list.component.html',
  styleUrls: ['./expense-list.component.scss'],
})
export class ExpenseListComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  private filter = {category: '', expense: ''};
  public categoryFilterList = [];

  private categoryList = [];
  private expenseList;
  public isLoadData = false;

  constructor(public dialog: MatDialog, private api: ApiService) {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();

    this.categoryList = JSON.parse(localStorage.getItem('json_expense_category'));
    this.isLoadData = true;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: 'CODE',
        field: 'id',
        sort: 'asc',
        type: ['xSmallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'EXPENSE CATEGORY',
        field: 'id',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
        valueFormatter: params => params.value ? this.categoryList.find(item => item.code === params.value.slice(0, 1)).description : '-'
      },
      {
        headerName: 'NAME OF PORT EXPENSE',
        field: 'description',
        type: ['xxxLargeColumn', 'stringColumn'],
      },
      {
        headerName: 'DEL',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: () => `<i class="fa fa-trash-o"></i>`
      },
    ];
  }

  onRowReady() {
    this.api.getExpenseList().then(data => {
      this.expenseList = data.data;
      this.gridApi.setRowData(data.data);
      this.categoryFilterList = this.categoryList;
      this.onFilterUpdate();
    });
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked(event) {
    this.dialog.open(ExpenseListDialogComponent, {
      width: '600px', data: {data: event.data, expenseList: this.expenseList}
    }).afterClosed().subscribe(data => this.onRowReady());
  }

  onCellClicked(event) {
    if (event.colDef.headerName === 'DEL') {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'warning', message: 'Do you want to delete ' + event.data.id + ' from the Port Expense List?'}
      }).afterClosed().subscribe(result => {
        if (result) {
          this.api.deleteExpenseList(event.data.id).then(() => {
            this.onRowReady();
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'success', message: event.data.id + ' has been deleted.'}
            });
          }, error => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
            });
          });
        }
      });
    }
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(value): void {
    this.filter[value.group] = value.value;
    this.onFilterUpdate();
  }

  onFilterUpdate(): void {
    this.gridApi.setFilterModel({
      id: {type: 'startsWith', filter: this.filter.category},
      description: {type: 'startsWith', filter: this.filter.expense},
    });
    this.gridApi.onFilterChanged();
  }

  /////////////////// Click Event ///////////////////

  onClickAddNewExpense(): void {
    this.dialog.open(ExpenseListDialogComponent, {
      width: '600px', data: {data: [], expenseList: this.expenseList}
    }).afterClosed().subscribe(() => this.onRowReady());
  }
}
