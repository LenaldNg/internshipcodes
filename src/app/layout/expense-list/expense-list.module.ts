import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';
import {ExpenseListRoutingModule} from './expense-list-routing.module';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {ExpenseListComponent} from './expense-list.component';
import {ComponentModule} from '../../shared/component.module';
import {ApiService} from '../../services/api.service';
import {ExpenseListDialogComponent} from '../../dialog/expense-list-dialog/expense-list-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    ExpenseListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    ExpenseListComponent,
    ExpenseListDialogComponent
  ],
  providers: [
    ApiService
  ],
  entryComponents: [
    ExpenseListDialogComponent
  ]
})
export class ExpenseListModule {
}
