import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MessageDialogComponent} from '../../dialog/message-dialog/message-dialog.component';
import {AdminDialogComponent} from '../../dialog/admin-dialog/admin-dialog.component';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  private filter = {name: '', role: ''};
  public roleFilterList = [];

  public companyList = [];
  public roleList = [];
  public isLoadData = false;

  constructor(public dialog: MatDialog, private api: ApiService) {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();

    this.api.getCompany().then(res => {
      res.data.forEach(data => this.companyList.push(data));
      this.roleList = JSON.parse(localStorage.getItem('json_roles'));
      this.isLoadData = true;
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: 'FIRST NAME',
        field: 'first_name',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'LAST NAME',
        field: 'last_name',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'E-MAIL',
        field: 'email',
        type: ['xLargeColumn', 'stringColumn'],
      },
      {
        headerName: 'CODE',
        field: 'phone_country_code',
        type: ['xSmallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'MOBILE NO.',
        field: 'phone_number',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'COMPANY NAME',
        field: 'company_id',
        type: ['xLargeColumn'],
        valueFormatter: params => params.value ? this.companyList.find(item => item.id === Number(params.value)).name : '-'
      },
      {
        headerName: 'USER ROLE',
        field: 'role_id',
        sort: 'asc',
        type: ['smallColumn', 'centerColumn'],
        valueFormatter: params => params.value ? this.roleList.find(item => item.id === Number(params.value)).description : '-'
      },
      {
        headerName: 'RST',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: params => `<i class="fa fa-refresh"></i>`
      },
      {
        headerName: 'DEL',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: params => params.data.role_id === 1 ? `<i class="fa fa-trash-o disabled"></i>` : `<i class="fa fa-trash-o"></i>`
      },
    ];
  }

  onRowReady() {
    this.api.getUserAdmin().then(res => {
      this.gridApi.setRowData(res.data);

      this.roleFilterList = [];
      this.gridApi.forEachNodeAfterFilterAndSort(node => {
        this.roleFilterList.push({id: node.data.role_id, value: this.roleList.find(item => item.id === Number(node.data.role_id)).description});
      });
      this.roleFilterList = CommonCode.filterAndSortList(this.roleFilterList, 'value');
      this.onFilterUpdate();
    });
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked(event) {
    this.dialog.open(AdminDialogComponent, {
      width: '600px', data: event.data
    }).afterClosed().subscribe(data => this.onRowReady());
  }

  onCellClicked(event) {
    if (event.colDef.headerName === 'RST') {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'warning', message: 'Do you want to reset User ' + event.data.first_name.toUpperCase() + '\'s password?'}
      }).afterClosed().subscribe(result => {
        if (result) {
          event.data.password = 'baycrest000';
          this.api.updateUserAdmin(event.data.id, event.data).then(res => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'success', message: 'User ' + res.data.first_name.toUpperCase() + '\'s password has been reset.'}
            });
          }, error => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
            });
          });
        }
      });
    } else if (event.colDef.headerName === 'DEL' && event.data.role_id !== 1) {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'warning', message: 'Do you want to delete User ' + event.data.first_name.toUpperCase() + '\'s account?'}
      }).afterClosed().subscribe(result => {
        if (result) {
          this.api.deleteUserAdmin(event.data.id).then(res => {
            this.onRowReady();
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'success', message: 'User ' + res.data.first_name.toUpperCase() + '\'s account has been deleted.'}
            });
          }, error => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
            });
          });
        }
      });
    }
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(value): void {
    this.filter[value.group] = value.value;
    this.onFilterUpdate();
  }

  onFilterUpdate(): void {
    this.gridApi.setFilterModel({
      first_name: {type: 'startsWith', filter: this.filter.name},
      role_id: {type: 'startsWith', filter: this.filter.role},
    });
    this.gridApi.onFilterChanged();
  }

  /////////////////// Click Event ///////////////////

  onClickAddNewUser(): void {
    this.dialog.open(AdminDialogComponent, {
      width: '600px', data: []
    }).afterClosed().subscribe(() => this.onRowReady());
  }
}
