import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AdminRoutingModule} from './admin-routing.module';
import {AdminComponent} from './admin.component';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {AgGridModule} from 'ag-grid-angular';
import {AdminDialogComponent} from '../../dialog/admin-dialog/admin-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentModule} from '../../shared/component.module';
import {ApiService} from '../../services/api.service';
import {SupportService} from '../../services/support.service';

@NgModule({
  imports: [
    CommonModule,
    AdminRoutingModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    AdminComponent,
    AdminDialogComponent
  ],
  providers: [
    ApiService,
    SupportService
  ],
  entryComponents: [
    AdminDialogComponent
  ]
})
export class AdminModule {
}
