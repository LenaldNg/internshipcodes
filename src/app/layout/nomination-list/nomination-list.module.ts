import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgGridModule} from 'ag-grid-angular';
import {ApiService} from '../../services/api.service';
import {NominationListRoutingModule} from './nomination-list-routing.module';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {ComponentModule} from '../../shared/component.module';
import {NominationListComponent} from './nomination-list.component';
import {NominationDialogComponent} from '../../dialog/nomination-dialog/nomination-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    NominationListRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    NominationListComponent,
    NominationDialogComponent
  ],
  providers: [
    ApiService,
  ],
  entryComponents: [
    NominationDialogComponent
  ]
})
export class NominationListModule {
}
