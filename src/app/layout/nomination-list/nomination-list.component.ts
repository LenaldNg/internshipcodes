import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {ApiService} from '../../services/api.service';
import * as moment from 'moment';
import {NominationDialogComponent} from '../../dialog/nomination-dialog/nomination-dialog.component';
import {Router} from '@angular/router';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-nomination-list',
  templateUrl: 'nomination-list.component.html',
  styleUrls: ['./nomination-list.component.scss'],
})
export class NominationListComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  private filter = {status: '', owner: '', port: ''};
  public statusFilterList = [];
  public ownerFilterList = [];
  public portFilterList = [];

  private companyList;
  private vesselList;
  private userList;
  public isLoadData = false;

  constructor(private router: Router, public dialog: MatDialog, private api: ApiService) {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();

    this.api.getCompany().then(res => {
      this.companyList = res.data;

      this.api.getVesselList().then(data => {
        this.vesselList = data.data;

        this.api.getUserAdmin().then(obj => {
          this.userList = obj.data;
          this.isLoadData = true;
        });
      });
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: 'DATE',
        field: 'date',
        type: ['smallColumn', 'centerColumn'],
        valueFormatter: params => params.value ? moment.unix(params.value).format('DD MMM') : '',
      },
      {
        headerName: 'OWNER',
        field: 'owner_company_id',
        type: ['largeColumn'],
        valueFormatter: params => this.companyList.find(item => item.id === Number(params.value)).name
      },
      {
        headerName: 'VESSEL',
        field: 'vessel_id',
        type: ['largeColumn'],
        valueFormatter: params => this.vesselList.find(item => item.id === Number(params.value)).name
      },
      {
        headerName: 'VOY',
        field: 'voyage_number',
        type: ['smallColumn', 'centerColumn'],
      },
      {
        headerName: 'COUNTRY',
        field: 'country',
        type: ['middleColumn'],
      },
      {
        headerName: 'PORT',
        field: 'port',
        type: ['middleColumn'],
      },
      {
        headerName: 'AGENT',
        field: 'agent_company_id',
        type: ['largeColumn'],
        valueFormatter: params => this.companyList.find(item => item.id === Number(params.value)).name
      },
      {
        headerName: 'PURPOSE',
        field: 'purposes',
        type: ['middleColumn'],
      },
      {
        headerName: 'ARR',
        field: 'date_arrival',
        type: ['smallColumn', 'centerColumn'],
        valueFormatter: params => params.value ? moment.unix(params.value).format('DD MMM') : '',
      },
      {
        headerName: 'DEP',
        field: 'date_departure',
        type: ['smallColumn', 'centerColumn'],
        valueFormatter: params => params.value ? moment.unix(params.value).format('DD MMM') : '',
      },
      {
        headerName: 'EPDA',
        field: 'sum_epda',
        type: ['middleColumn', 'rightColumn'],
        cellRenderer: params => params.value ? 'US$ ' + CommonCode.decimalNumberFormat(params.value, 2) : '-',
      },
      {
        headerName: 'FDA',
        field: 'sum_fda',
        type: ['middleColumn', 'rightColumn'],
        cellRenderer: params => params.value ? 'US$ ' + CommonCode.decimalNumberFormat(params.value, 2) : '-',
      },
      {
        headerName: 'STATUS',
        field: 'status',
        type: ['middleColumn', 'centerColumn'],
      },
      {
        headerName: 'DEL',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: () => `<i class="fa fa-trash-o"></i>`
      },
    ];
  }

  onRowReady() {
    this.api.getNominationList().then(data => {
      console.log(data.data);
      this.gridApi.setRowData(data.data);
      this.onFilterUpdate();
    });
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked(event) {
    this.api.getNominationItem(event.data.id).then(res => {
      localStorage.setItem('current_nomination_detail', JSON.stringify(res.data));
      this.router.navigate([`./detail/nomination$${event.data.id}`]);
    });
  }

  onCellClicked(event) {
    if (event.colDef.headerName === 'DEL') {
      // this.dialog.open(MessageDialogComponent, {
      //   width: '600px', data: {status: 'warning', message: 'Do you want to delete ' + event.data.id + ' from the Port Expense List?'}
      // }).afterClosed().subscribe(result => {
      //   if (result) {
      //     this.api.deleteExpenseList(event.data.id).then(() => {
      //       this.onRowReady();
      //       this.dialog.open(MessageDialogComponent, {
      //         width: '600px', data: {status: 'success', message: event.data.id + ' has been deleted.'}
      //       });
      //     }, error => {
      //       this.dialog.open(MessageDialogComponent, {
      //         width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      //       });
      //     });
      //   }
      // });
    }
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(value): void {
    this.filter[value.group] = value.value;
    this.onFilterUpdate();
  }

  onFilterUpdate(): void {
    this.gridApi.setFilterModel({
      status: {type: 'startsWith', filter: this.filter.status},
      owner_company_id: {type: 'startsWith', filter: this.filter.owner},
      port: {type: 'startsWith', filter: this.filter.port},
    });
    this.gridApi.onFilterChanged();

    this.statusFilterList = [];
    this.ownerFilterList = [];
    this.portFilterList = [];

    this.gridApi.forEachNodeAfterFilterAndSort(node => {
      this.statusFilterList.push({id: node.data.status, value: node.data.status});
      this.ownerFilterList.push({id: node.data.owner_company_id, value: this.companyList.find(item => item.id === Number(node.data.owner_company_id)).name});
      this.portFilterList.push({id: node.data.port, value: node.data.port});
    });

    this.statusFilterList = CommonCode.filterAndSortList(this.statusFilterList, 'value');
    this.ownerFilterList = CommonCode.filterAndSortList(this.ownerFilterList, 'value');
    this.portFilterList = CommonCode.filterAndSortList(this.portFilterList, 'value');
  }

  /////////////////// Click Event ///////////////////

  onClickAddNewNomination(): void {
    this.dialog.open(NominationDialogComponent, {
      width: '600px', data: {data: [], companyList: this.companyList, userList: this.userList, vesselList: this.vesselList}
    }).afterClosed().subscribe(() => this.onRowReady());
  }
}
