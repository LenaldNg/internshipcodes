import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {NominationListComponent} from './nomination-list.component';

const routes: Routes = [
  {
    path: '', component: NominationListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NominationListRoutingModule {
}
