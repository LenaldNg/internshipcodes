import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {VesselComponent} from './vessel.component';

const routes: Routes = [
  {
    path: '',
    component: VesselComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VesselRoutingModule {
}
