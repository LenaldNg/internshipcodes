import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {VesselDialogComponent} from '../../dialog/vessel-dialog/vessel-dialog.component';
import {MessageDialogComponent} from '../../dialog/message-dialog/message-dialog.component';
import {ExcelService} from '../../services/excel.service';
import * as moment from 'moment';
import * as Excel from 'exceljs/dist/exceljs.min.js';
import {SupportService} from '../../services/support.service';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-vessel-page',
  templateUrl: './vessel.component.html',
  styleUrls: ['./vessel.component.scss']
})
export class VesselComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  private filter = {vessel: '', company_id: ''};
  public ownerFilterList = [];

  private companyList = [];
  public isLoadData = false;

  constructor(public dialog: MatDialog, private support: SupportService, private api: ApiService, private excel: ExcelService) {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();

    this.support.getCompanyList().then(res => {
      res.data.forEach(item => this.companyList.push(item));
      this.isLoadData = true;
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: 'OWNER NAME',
        field: 'company_id',
        type: ['largeColumn', 'stringColumn'],
        valueFormatter: params => this.companyList.find(item => item.id === Number(params.value)).name
      },
      {
        headerName: 'VESSEL NAME',
        field: 'name',
        type: ['largeColumn', 'stringColumn'],
      },
      {
        headerName: 'DWT (MT)',
        field: 'dwt',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'GRT',
        field: 'grt',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'LOA (M)',
        field: 'loa',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'BEAM (M)',
        field: 'beam',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'DRAFT (M)',
        field: 'draft',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'SEG',
        field: 'seg',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'COATING',
        field: 'coating',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'CAPACITY',
        field: 'capacity',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'BUILT',
        field: 'built',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
        valueFormatter: params => params.value ? moment.unix(params.value).format('YYYY - MM') : '',
      },
      {
        headerName: 'CLASS',
        field: 'vessel_class',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'FLAG',
        field: 'flag',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'DEL',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: params => `<i class="fa fa-trash-o"></i>`
      },
    ];
  }

  onRowReady() {
    this.api.getVesselList().then(data => {
      console.log(data.data);
      this.gridApi.setRowData(data.data);
      this.ownerFilterList = [];
      this.gridApi.forEachNodeAfterFilterAndSort(node => {
        this.ownerFilterList.push({id: node.data.company_id, value: this.companyList.find(item => item.id === Number(node.data.company_id)).name});
      });
      this.ownerFilterList = CommonCode.filterAndSortList(this.ownerFilterList, 'value');
      this.onFilterUpdate();
    });
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked(event) {
    this.dialog.open(VesselDialogComponent, {
      width: '600px', height: '670px', data: event.data
    }).afterClosed().subscribe(() => this.onRowReady());
  }

  onCellClicked(event) {
    if (event.colDef.headerName === 'DEL') {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'warning', message: 'Do you want to delete Vessel ' + event.data.name.toUpperCase() + '?'}
      }).afterClosed().subscribe(result => {
        if (result) {
          this.api.deleteVessel(event.data.id).then(res => {
            this.onRowReady();
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'success', message: 'Vessel ' + res.data.name.toUpperCase() + ' has been deleted.'}
            });
          }, error => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
            });
          });
        }
      });
    }
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(value): void {
    this.filter[value.group] = value.value;
    this.onFilterUpdate();
  }

  onFilterUpdate(): void {
    this.gridApi.setFilterModel({
      name: {type: 'startsWith', filter: this.filter.vessel},
      company_id: {type: 'startsWith', filter: this.filter.company_id},
    });
    this.gridApi.onFilterChanged();
  }

  /////////////////// Click Event ///////////////////

  onClickAddNewVessel(): void {
    this.dialog.open(VesselDialogComponent, {
      width: '600px', height: '670px', data: []
    }).afterClosed().subscribe(() => this.onRowReady());
  }

  onClickExcel(): void {
    const workSheetName = 'FLEET LIST';
    const companyName = 'BAYCREST';
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet(workSheetName);

    worksheet.columns = [
      {header: '#', key: 'id', width: 6},
      {header: 'OWNER NAME', key: 'company_id', width: 25},
      {header: 'VESSEL NAME', key: 'name', width: 25},
      {header: 'DWT (MT)', key: 'dwt', width: 15},
      {header: 'GRT', key: 'grt', width: 15},
      {header: 'LOA (M)', key: 'loa', width: 15},
      {header: 'BEAM (M)', key: 'beam', width: 15},
      {header: 'DRAFT (M)', key: 'draft', width: 15},
      {header: 'SEG', key: 'seg', width: 8},
      {header: 'COATING', key: 'coating', width: 20},
      {header: 'CAPACITY', key: 'capacity', width: 15},
      {header: 'BUILT', key: 'built', width: 15},
      {header: 'CLASS', key: 'vessel_class', width: 8},
      {header: 'FLAG', key: 'flag', width: 20},
    ];

    this.gridApi.forEachNodeAfterFilterAndSort(node => {
      worksheet.addRow({
        id: node.rowIndex + 1,
        company_id: this.companyList.find(item => item.id === Number(node.data.company_id)).name,
        name: node.data.name,
        dwt: node.data.dwt,
        grt: node.data.grt,
        loa: node.data.loa,
        beam: node.data.beam,
        draft: node.data.draft,
        seg: node.data.seg,
        coating: node.data.coating,
        capacity: node.data.capacity,
        built: moment.unix(node.data.built).format('YYYY - MM'),
        vessel_class: node.data.vessel_class,
        flag: node.data.flag,
      });
    });

    this.excel.createExcel(workbook, companyName, workSheetName);
  }

  onClickPDF(): void {

  }

  onClickEmail(): void {

  }
}
