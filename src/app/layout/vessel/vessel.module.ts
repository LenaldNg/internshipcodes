import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {VesselComponent} from './vessel.component';
import {VesselRoutingModule} from './vessel-routing.module';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {AgGridModule} from 'ag-grid-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentModule} from '../../shared/component.module';
import {VesselDialogComponent} from '../../dialog/vessel-dialog/vessel-dialog.component';
import {ExcelService} from '../../services/excel.service';
import {ApiService} from '../../services/api.service';

@NgModule({
  imports: [
    CommonModule,
    VesselRoutingModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    VesselComponent,
    VesselDialogComponent
  ],
  providers: [
    ApiService,
    ExcelService
  ],
  entryComponents: [
    VesselDialogComponent
  ]
})
export class VesselModule {
}
