import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {MessageDialogComponent} from '../../dialog/message-dialog/message-dialog.component';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services/auth.service';
import {PasswordValidation} from '../../shared/password-validation';
import {Router} from '@angular/router';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent {
  public companyForm: FormGroup;
  public userForm: FormGroup;
  public passwordForm: FormGroup;

  public countryList = [];

  constructor(private fb: FormBuilder, private auth: AuthService, private dialog: MatDialog, private router: Router) {
    const company = JSON.parse(localStorage.getItem('user_company'));
    const role = JSON.parse(localStorage.getItem('user_role'));

    this.companyForm = fb.group({
      registration_number: new FormControl({value: company.registration_number, disabled: true}),
      name: new FormControl({value: company.name, disabled: true}),
      address_1: new FormControl({value: company.address_1, disabled: true}),
      address_2: new FormControl({value: company.address_2, disabled: true}),
      building: new FormControl({value: company.building, disabled: true}),
      postal_code: new FormControl({value: company.postal_code, disabled: true}),
      country_code: new FormControl({value: company.country_code, disabled: true}),
      phone_number: new FormControl({value: company.phone_number, disabled: true}),
    });

    this.userForm = fb.group({
      id: new FormControl(localStorage.getItem('user_id')),
      first_name: new FormControl({value: localStorage.getItem('user_first_name'), disabled: true}),
      last_name: new FormControl({value: localStorage.getItem('user_last_name'), disabled: true}),
      email: new FormControl({value: localStorage.getItem('user_email'), disabled: true}),
      phone_country_code: new FormControl(localStorage.getItem('user_phone_country_code'), [Validators.required]),
      phone_number: new FormControl(localStorage.getItem('user_phone_number'), [Validators.required, Validators.min(0)]),
      company_id: new FormControl(company.id),
      role_id: new FormControl(role.id),
    });

    this.passwordForm = fb.group({
      old_password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      new_password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      confirm_password: new FormControl('', [Validators.required]),
    }, {
      validator: PasswordValidation.MatchPassword
    });

    this.countryList = CommonCode.filterAndSortList(JSON.parse(localStorage.getItem('json_country')), 'dial_code');
  }

  onClickUpdatePICProfile(): void {
    this.auth.updateUser(this.userForm.getRawValue()).then(() => {
      localStorage.setItem('user_phone_country_code', this.userForm.controls['phone_country_code'].value);
      localStorage.setItem('user_phone_number', this.userForm.controls['phone_number'].value);
      this.dialog.open(MessageDialogComponent, {
        width: '600px',
        data: {
          status: 'success',
          message: 'The PHONE NUMBER has been updated to +' + this.userForm.controls['phone_country_code'].value + ' ' + this.userForm.controls['phone_number'].value
        }
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
      });
    });
  }

  onClickUpdatePassword(): void {
    this.auth.updatePassword({current_password: this.passwordForm.value.old_password, new_password: this.passwordForm.value.new_password}).then(() => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'success', message: 'The password has been changed. Please re-login to the system.'}
      }).afterClosed().subscribe(() => {
        Object.keys(localStorage).forEach(key => {
          if (!(key === 'auth_email' || key === 'auth_password' || key === 'auth_remember')) {
            localStorage.removeItem(key);
          }
        });
        this.router.navigate(['/login']);
      });
    }, error => {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'fail', message: 'Your current password is wrong.'}
      });
    });
  }
}
