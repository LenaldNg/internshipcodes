import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {AgGridModule} from 'ag-grid-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentModule} from '../../shared/component.module';
import {ProfileRoutingModule} from './profile-routing.module';
import {ProfileComponent} from './profile.component';

@NgModule({
  imports: [
    CommonModule,
    ProfileRoutingModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    ProfileComponent,
  ],
})
export class ProfileModule {
}
