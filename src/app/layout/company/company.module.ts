import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CompanyRoutingModule} from './company-routing.module';
import {CompanyComponent} from './company.component';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {AgGridModule} from 'ag-grid-angular';
import {ReactiveFormsModule} from '@angular/forms';
import {ComponentModule} from '../../shared/component.module';
import {CompanyDialogComponent} from '../../dialog/company-dialog/company-dialog.component';
import {ApiService} from '../../services/api.service';
import {ColorPickerModule} from 'ngx-color-picker';

@NgModule({
  imports: [
    CommonModule,
    CompanyRoutingModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    ColorPickerModule,
    AgGridModule.withComponents([])
  ],
  declarations: [
    CompanyComponent,
    CompanyDialogComponent,
  ],
  providers: [
    ApiService
  ],
  entryComponents: [
    CompanyDialogComponent
  ]
})
export class CompanyModule {
}
