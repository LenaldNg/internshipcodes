import {Component} from '@angular/core';
import {MatDialog} from '@angular/material';
import {CompanyDialogComponent} from '../../dialog/company-dialog/company-dialog.component';
import {MessageDialogComponent} from '../../dialog/message-dialog/message-dialog.component';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-company-page',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  private filter = {type: '', name: ''};
  public typeFilterList = [];

  public countryList = [];
  public typeList = [];
  public isLoadData = false;

  constructor(public dialog: MatDialog, private api: ApiService) {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();

    this.countryList = JSON.parse(localStorage.getItem('json_country'));
    this.typeList = JSON.parse(localStorage.getItem('json_company_type'));
    this.isLoadData = true;
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: ' ',
        field: 'color_code',
        type: ['colorColumn'],
        cellRenderer: params => `<i class="fa fa-square"></i>`,
        cellStyle: params => {
          return {color: '#' + params.value};
        }
      },
      {
        headerName: 'COMPANY NAME',
        field: 'name',
        type: ['xLargeColumn', 'stringColumn'],
      },
      {
        headerName: 'TYPE',
        field: 'type_id',
        sort: 'asc',
        type: ['middleColumn', 'stringColumn'],
        valueFormatter: params => this.typeList.find(item => item.id === Number(params.value)).description
      },
      {
        headerName: 'REGISTRATION NO.',
        field: 'registration_number',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'COUNTRY',
        field: 'country_code',
        type: ['middleColumn', 'stringColumn'],
        valueFormatter: params => this.countryList.find(item => item.code === params.value).name
      },
      {
        headerName: 'PORT',
        field: 'port',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'WORK ADDRESS',
        field: 'address_1',
        type: ['xLargeColumn', 'stringColumn'],
      },
      {
        headerName: 'POSTAL CODE',
        field: 'postal_code',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'WORK PHONE',
        field: 'phone_number',
        type: ['smallColumn', 'centerColumn', 'stringColumn'],
      },
      {
        headerName: 'DA FEE (US$)',
        field: 'da_fee',
        type: ['smallColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'BALANCE DA (US$)',
        field: 'da_balance',
        type: ['middleColumn', 'rightColumn', 'numberColumn'],
      },
      {
        headerName: 'DEL',
        type: ['iconColumn', 'centerColumn'],
        cellRenderer: () => `<i class="fa fa-trash-o"></i>`
      },
    ];
  }

  onRowReady() {
    this.api.getCompany().then(data => {
      console.log(data.data);
      this.gridApi.setRowData(data.data);
      this.gridApi.forEachNodeAfterFilterAndSort(node => {
        this.typeFilterList.push({id: node.data.type_id, value: this.typeList.find(item => item.id === Number(node.data.type_id)).description});
      });
      this.typeFilterList = CommonCode.filterAndSortList(this.typeFilterList, 'value');
      this.onFilterUpdate();
    });
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }

  onRowDoubleClicked(event) {
    this.dialog.open(CompanyDialogComponent, {
      width: '600px', data: event.data
    }).afterClosed().subscribe(data => this.onRowReady());
  }

  onCellClicked(event) {
    if (event.colDef.headerName === 'DEL') {
      this.dialog.open(MessageDialogComponent, {
        width: '600px', data: {status: 'warning', message: 'Do you want to delete Company ' + event.data.name.toUpperCase() + '?'}
      }).afterClosed().subscribe(result => {
        if (result) {
          this.api.deleteCompany(event.data.id).then(() => {
            this.onRowReady();
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'success', message: 'Company ' + event.data.name.toUpperCase() + ' has been deleted.'}
            });
          }, error => {
            this.dialog.open(MessageDialogComponent, {
              width: '600px', data: {status: 'fail', message: JSON.parse(error._body).message}
            });
          });
        }
      });
    }
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(value): void {
    this.filter[value.group] = value.value;
    this.onFilterUpdate();
  }

  onFilterUpdate(): void {
    this.gridApi.setFilterModel({
      type_id: {type: 'startsWith', filter: this.filter.type},
      name: {type: 'startsWith', filter: this.filter.name},
    });
    this.gridApi.onFilterChanged();
  }

  /////////////////// Click Event ///////////////////

  onClickAddNewCompany(): void {
    this.dialog.open(CompanyDialogComponent, {
      width: '600px', data: []
    }).afterClosed().subscribe(() => this.onRowReady());
  }
}
