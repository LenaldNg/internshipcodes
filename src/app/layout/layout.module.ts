import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutComponent} from './layout.component';
import {LayoutRoutingModule} from './layout-routing.module';
import {AngularMaterialModule} from '../shared/angular-material.module';

@NgModule({
  imports: [
    CommonModule,
    LayoutRoutingModule,
    AngularMaterialModule
  ],
  declarations: [
    LayoutComponent,
  ],
  providers: []
})
export class LayoutModule {
}
