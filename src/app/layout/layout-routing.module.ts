import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LayoutComponent} from './layout.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: 'admin'},
      {path: 'admin', loadChildren: './admin/admin.module#AdminModule'},
      {path: 'company', loadChildren: './company/company.module#CompanyModule'},
      {path: 'vessel', loadChildren: './vessel/vessel.module#VesselModule'},
      {path: 'profile', loadChildren: './profile/profile.module#ProfileModule'},
      {path: 'expense-list', loadChildren: './expense-list/expense-list.module#ExpenseListModule'},
      {path: 'nomination-list', loadChildren: './nomination-list/nomination-list.module#NominationListModule'},
      {path: 'detail/:id', loadChildren: './detail/detail.module#DetailModule'},
      {path: 'tracking', loadChildren: './tracking/tracking.module#TrackingModule'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
