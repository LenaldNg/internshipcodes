import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TrackingRoutingModule} from './tracking-routing.module';
import {TrackingComponent} from './tracking.component';
import {ApiService} from '../../services/api.service';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {ComponentModule} from '../../shared/component.module';
import {ShipService} from '../../services/ship.service';

@NgModule({
  imports: [
    CommonModule,
    TrackingRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule
  ],
  declarations: [
    TrackingComponent,
  ],
  providers: [
    ApiService,
    ShipService
  ],
})
export class TrackingModule {
}
