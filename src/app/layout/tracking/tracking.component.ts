import {Component, OnInit} from '@angular/core';
import ol from 'openlayers/dist/ol';
import {FormControl} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {ShipService} from '../../services/ship.service';

@Component({
  selector: 'app-tracking',
  templateUrl: 'tracking.component.html',
  styleUrls: ['./tracking.component.scss'],
})
export class TrackingComponent implements OnInit {
  private map;

  private filter = {owner: '', vessel: ''};
  public ownerFilterList = [];
  public vesselFilterList = [];
  public ownerControl = new FormControl('');
  public vesselControl = new FormControl('');

  constructor(private api: ApiService, private ship: ShipService) {

  }

  ngOnInit() {
    this.showMap();

    this.api.getVesselList().then(res => {
      this.vesselFilterList = res.data;

      this.api.getCompany().then(obj => {
        this.ownerFilterList = obj.data;

        this.vesselFilterList.forEach(data => {
          data.color_code = this.ownerFilterList.find(elem => elem.id === data.company_id).color_code;
          this.showShip(data.mmsi, data.color_code);
        });
      });
    });
  }

  /////////////////// display the map and vessel position ///////////////////

  showMap(): any {
    this.map = new ol.Map({
      target: 'sea-map',
      layers: [new ol.layer.Tile({
        source: new ol.source.OSM({
          url: 'https://cartodb-basemaps-c.global.ssl.fastly.net/rastertiles/voyager/{z}/{x}/{y}.png',
          crossOrigin: null,
          cacheSize: 16384,
          attributions: [new ol.Attribution({html: 'ShipsFocus Pte Ltd'})],
        }),
      })],
      view: new ol.View({
        center: ol.proj.fromLonLat([115.26, 20.12]),
        zoom: 4,
        minZoom: 3,
      }),
      controls: []
    });

    return this;
  }

  showShip(mmsi, color_code): void {
    this.ship.getAIS([{mmsi: mmsi, count: '3d'}]).then(res => {
      console.log(res);
      if (res[0]) {
        const position = res[0].positions;
        for (let i = 0; i < position.length; i++) {
          if (position[i + 1]) {
            const lng = parseFloat(position[i].lng);
            const lat = parseFloat(position[i].lat);
            const next_lng = parseFloat(position[i + 1].lng);
            const next_lat = parseFloat(position[i + 1].lat);
            const heading = parseFloat(position[i].hdg);
            const rotation = !isNaN(heading) ? heading * Math.PI / 180 : 0;

            if (i === 0) {
              const vesselLayer = new ol.layer.Vector({
                title: 'markers',
                source: new ol.source.Vector({
                  features: [new ol.Feature({
                    geometry: new ol.geom.Point(ol.proj.fromLonLat([lng, lat])),
                  })]
                }),
                style: new ol.style.Style({
                  text: new ol.style.Text({
                    text: '\u0023',
                    font: '100 30px sficons',
                    fill: new ol.style.Fill({color: '#' + color_code}),
                    stroke: new ol.style.Stroke({color: 'black', width: 2}),
                    rotateWithView: true,
                    rotation,
                  })
                })
              });
              vesselLayer.setZIndex(99);
              this.map.addLayer(vesselLayer);
            }

            const layerLines = new ol.layer.Vector({
              source: new ol.source.Vector({
                features: [new ol.Feature({
                  geometry: new ol.geom.LineString([ol.proj.fromLonLat([lng, lat]), ol.proj.fromLonLat([next_lng, next_lat])]),
                  name: 'Line',
                })]
              }),
              style: new ol.style.Style({
                fill: new ol.style.Fill({color: '#' + color_code}),
                stroke: new ol.style.Stroke({color: '#' + color_code, width: 2}),
              })
            });
            this.map.addLayer(layerLines);
          }
        }
      }
    });
  }

  /////////////////// Filter Function ///////////////////

  onFilterChange(): void {
    this.filter.owner = this.ownerControl.value;
    this.filter.vessel = this.vesselControl.value;
  }
}
