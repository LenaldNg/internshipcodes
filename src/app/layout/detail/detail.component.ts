import {Component} from '@angular/core';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ApiService} from '../../services/api.service';
import {CommonCode} from '../../shared/common-code';

@Component({
  selector: 'app-vessel-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['./detail.component.scss'],
})
export class DetailComponent {
  public detailForm: FormGroup;
  public data;

  public vesselList = [];
  public ownerList = [];
  public agentList = [];
  public picList = [];
  public isPortSelected = false;
  public isLoadData = false;

  constructor(private fb: FormBuilder, private api: ApiService) {
    this.data = JSON.parse(localStorage.getItem('current_nomination_detail'));

    console.log(this.data);

    this.api.getCompany().then(res => res.data.forEach(r => {
      if (r.type_id === 5) {
        this.ownerList.push(r);
      } else if (r.type_id === 3) {
        this.agentList.push(r);
      }
    }));

    this.api.getVesselList().then(elem => elem.data.forEach(e => {
      if (e.company_id === this.data.owner_company_id) {
        this.vesselList.push(e);
      }
    }));

    this.api.getUserAdmin().then(obj => obj.data.forEach(o => {
      if (o.company_id === this.data.owner_company_id) {
        this.picList.push(o);
      }
    }));

    this.api.getExpenseList().then(res => {
      localStorage.setItem('current_expense_list', JSON.stringify(res.data));
      this.isLoadData = true;
    });

    this.isPortSelected = !!this.data.port;

    this.detailForm = fb.group({
      vessel_id: new FormControl(this.data.vessel_id),
      voyage_number: new FormControl(this.data.voyage_number),
      port: new FormControl(this.data.port),
      purposes: new FormControl(this.data.purposes),
      date_arrival: new FormControl({value: CommonCode.unixToDate(this.data.date_arrival, ''), disabled: true}),
      date_departure: new FormControl({value: CommonCode.unixToDate(this.data.date_departure, ''), disabled: true}),
      agent_company_id: new FormControl(this.data.agent_company_id),
      owner_company_id: new FormControl(this.data.owner_company_id),
      pic_user_id: new FormControl(this.data.pic_user_id),
      date_epda: new FormControl({value: CommonCode.unixToDate(this.data.date_epda, ''), disabled: true}),
      date_fda: new FormControl({value: CommonCode.unixToDate(this.data.date_fda, ''), disabled: true}),
      ex_epda: new FormControl({value: CommonCode.decimalNumberFormat(this.data.ex_epda ? this.data.ex_epda : '0', 2), disabled: true}),
      sum_epda: new FormControl({value: CommonCode.decimalNumberFormat(this.data.sum_epda ? this.data.sum_epda : '0', 2), disabled: true}),
      sum_epda_us: new FormControl({value: CommonCode.decimalNumberFormat(this.data.sum_epda / this.data.ex_epda, 2), disabled: true}),
      ex_fda: new FormControl({value: CommonCode.decimalNumberFormat((Number(this.data.ex_atd) + Number(this.data.ex_ata)) / 2, 2), disabled: true}),
      sum_fda: new FormControl({value: CommonCode.decimalNumberFormat(this.data.sum_fda ? this.data.sum_fda : '0', 2), disabled: true}),
      sum_fda_us: new FormControl({value: CommonCode.decimalNumberFormat(this.data.sum_fda / ((Number(this.data.ex_atd) + Number(this.data.ex_ata)) / 2), 2), disabled: true}),
      da_balance: new FormControl({value: CommonCode.decimalNumberFormat('0', 2), disabled: true}),
      status: new FormControl(this.data.status)
    });
  }

  ///////////////// Data Emit /////////////////

  onValueSelected(value): void {
    this.isPortSelected = value;
  }
}
