import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DetailRoutingModule} from './detail-routing.module';
import {DetailComponent} from './detail.component';
import {AgGridModule} from 'ag-grid-angular';
import {ApiService} from '../../services/api.service';
import {AngularMaterialModule} from '../../shared/angular-material.module';
import {ComponentModule} from '../../shared/component.module';
import {PortExpenseComponent} from './port-expense/port-expense.component';
import {VesselExpenseComponent} from './vessel-expense/vessel-expense.component';
import {RenumerationComponent} from './renumeration/renumeration.component';
import {OwnerAccountComponent} from './owner-account/owner-account.component';
import {DatepickerEditorComponent} from '../../cell-editor/datepicker-editor/datepicker-editor.component';
import {OwlDateTimeModule, OwlNativeDateTimeModule} from 'ng-pick-datetime';

@NgModule({
  imports: [
    CommonModule,
    DetailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularMaterialModule,
    ComponentModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    AgGridModule.withComponents([
      DatepickerEditorComponent
    ])
  ],
  declarations: [
    DetailComponent,
    PortExpenseComponent,
    VesselExpenseComponent,
    RenumerationComponent,
    OwnerAccountComponent,
    DatepickerEditorComponent
  ],
  providers: [
    ApiService
  ],
})
export class DetailModule {
}
