import {Component} from '@angular/core';
import {CommonCode} from '../../../shared/common-code';

@Component({
  selector: 'app-renumeration-component',
  templateUrl: 'renumeration.component.html',
  styleUrls: ['./renumeration.component.scss'],
})
export class RenumerationComponent {
  public gridApi;
  public gridColumnApi;
  public icons;
  public columnDefs;
  public columnTypes;

  public data = JSON.parse(localStorage.getItem('current_nomination_detail'));

  constructor() {
    this.columnDefs = this.onColumnReady();
    this.columnTypes = CommonCode.agGridColumnTypes();
    this.icons = CommonCode.agGridIcon();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.onRowReady();
    params.api.sizeColumnsToFit();
  }

  onColumnReady() {
    return [
      {
        headerName: 'RENUMERATION',
        field: 'code',
        type: ['largeColumn', 'stringColumn'],
        valueFormatter: params => JSON.parse(localStorage.getItem('current_expense_list')).find(res => res.id === params.value).description
      },
      {
        headerName: 'VENDOR',
        field: 'vendor',
        type: ['middleColumn', 'stringColumn'],
      },
      {
        headerName: 'DATE',
        field: 'date',
        type: ['smallColumn', 'centerColumn'],
        valueFormatter: params => CommonCode.unixToDate(params.value, 'DD MMM YYYY')
      },
      {
        headerName: 'EPDA (S$)',
        field: 'epda_amt',
        type: ['smallColumn', 'rightColumn', 'stringColumn'],
        valueFormatter: params => params.value ? CommonCode.decimalNumberFormat(params.value, 2) : CommonCode.decimalNumberFormat(0, 2)
      },
      {
        headerName: 'FDA (S$)',
        field: 'fda_amt',
        type: ['smallColumn', 'rightColumn', 'stringColumn'],
        valueFormatter: params => params.value ? CommonCode.decimalNumberFormat(params.value, 2) : CommonCode.decimalNumberFormat(0, 2)
      },
      {
        headerName: 'DIFF (%)',
        field: 'difference',
        type: ['smallColumn', 'rightColumn'],
        valueGetter: params =>
          params.data.epda_amt ? CommonCode.decimalNumberFormat((params.data.fda_amt - params.data.epda_amt) / params.data.epda_amt * 100, 2)
            : CommonCode.decimalNumberFormat(0, 2)
      },
      {
        headerName: 'REMARKS',
        field: 'remarks',
        sort: 'asc',
        type: ['xxLargeColumn'],
      },
    ];
  }

  onRowReady() {
    const list = [];
    this.data.port_charges.forEach(res => {
      if (res.code.toLowerCase().includes('r')) {
        list.push(res);
      }
    });
    this.gridApi.setRowData(list);
  }

  onGridSizeChanged() {
    if (this.gridApi) {
      this.gridApi.sizeColumnsToFit();
    }
  }
}
