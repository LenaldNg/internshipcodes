import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './app-routing.module';
import {HttpModule} from '@angular/http';
import {AuthService} from './services/auth.service';
import {HttpClient} from './services/httpclient.class';
import {ReactiveFormsModule} from '@angular/forms';
import {PortService} from './services/port.service';
import {MessageDialogComponent} from './dialog/message-dialog/message-dialog.component';
import {AuthGuard} from './shared/auth.guard';
import {AngularMaterialModule} from './shared/angular-material.module';
import {SupportService} from './services/support.service';

@NgModule({
  declarations: [
    AppComponent,
    MessageDialogComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AngularMaterialModule,
    HttpModule,
  ],
  providers: [
    HttpClient,
    AuthService,
    PortService,
    SupportService,
    AuthGuard,
  ],
  bootstrap: [
    AppComponent
  ],
  entryComponents: [
    MessageDialogComponent
  ]
})
export class AppModule {
}
