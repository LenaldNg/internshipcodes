import {Injectable} from '@angular/core';
import {CanActivate, Router} from '@angular/router';
import {AuthService} from '../services/auth.service';
import {throwError} from 'rxjs/index';
import {map} from 'rxjs/operators';
import {catchError} from 'rxjs/internal/operators';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthService) {

  }

  canActivate() {
    if (localStorage.getItem('access_token')) {
      return this.auth.getMe().pipe(map(res => {
        console.log(res);
        if (res) {
          localStorage.setItem('user_id', res.data.id);
          localStorage.setItem('user_first_name', res.data.first_name);
          localStorage.setItem('user_last_name', res.data.last_name);
          localStorage.setItem('user_email', res.data.email);
          localStorage.setItem('user_phone_country_code', res.data.phone_country_code);
          localStorage.setItem('user_phone_number', res.data.phone_number);
          localStorage.setItem('user_company', JSON.stringify(res.data.company));
          localStorage.setItem('user_role', JSON.stringify(res.data.role));
          return true;
        } else {
          this.logout();
          return false;
        }
      }, () => {
        this.logout();
      }), catchError(error => {
        this.logout();
        return throwError(error);
      }));
    }
    this.logout();
    return false;
  }

  private logout() {
    Object.keys(localStorage).forEach(key => {
      if (!(key === 'auth_email' || key === 'auth_password' || key === 'auth_remember')) {
        localStorage.removeItem(key);
      }
    });
    this.router.navigate(['/login']);
  }
}
