import * as moment from 'moment';

export class CommonCode {

  /////////////////// AG Grid Function ///////////////////

  static agGridIcon() {
    return {
      filter: '<i class="fa fa-filter">'
    };
  }

  static agGridColumnTypes() {
    return {
      colorColumn: {width: 45, suppressSizeToFit: true},
      iconColumn: {width: 65, suppressSizeToFit: true},
      xSmallColumn: {width: 70},
      smallColumn: {width: 120},
      middleColumn: {width: 170},
      largeColumn: {width: 220},
      xLargeColumn: {width: 270},
      xxLargeColumn: {width: 320},
      xxxLargeColumn: {width: 500},

      centerColumn: {cellStyle: {textAlign: 'center'}},
      rightColumn: {cellStyle: {textAlign: 'right'}},
      boldColumn: {cellStyle: {fontWeight: 'bold'}},

      stringColumn: {valueFormatter: params => params.value ? params.value : '-'},
      numberColumn: {valueFormatter: params => params.value ? this.decimalNumberFormat(params.value, 2) : '-'},
    };
  }

  static onDefaultColumn() {
    return {
      enableCellChangeFlash: true,
    };
  }

  /////////////////// Filter And Sort Function ///////////////////

  static filterAndSortList(list, key) {
    return list.filter((elem, index, self) => index === this.deepIndexOf(self, elem)).sort((a, b) => {
      if (a[key] < b[key]) {
        return -1;
      }
      if (a[key] > b[key]) {
        return 1;
      }
      return 0;
    });
  }

  static filterAndSortListUsingSet(list) {
    return Array.from(new Set(list)).sort();
  }

  static sortList(list, key) {
    return list.sort((a, b) => {
      if (a[key] < b[key]) {
        return -1;
      }
      if (a[key] > b[key]) {
        return 1;
      }
      return 0;
    });
  }

  static deepIndexOf(self: any, elem: any) {
    return self.findIndex(cur => {
      return Object.keys(elem).every(key => {
        return elem[key] === cur[key];
      });
    });
  }

  /////////////////// Number Function ///////////////////

  static decimalNumberFormat(num, size) {
    const n = parseFloat(num).toFixed(size).split('.');
    return `${n[0].toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')}.${n[1] ? n[1] : (size === 2 ? '00' : '000')}`;
  }

  static pad(num, size): string {
    let s = num + '';
    while (s.length < size) {
      s = '0' + s;
    }
    return s;
  }

  static dateToUnit(date) {
    return Math.round(new Date(date).getTime() / 1000);
  }

  static unixToDate(unix, format) {
    return unix ? moment.unix(unix).format(format) : new Date();
  }
}
