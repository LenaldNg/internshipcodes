import {NgModule} from '@angular/core';
import {SelectFilterModule} from '../component/select-filter/select-filter.module';
import {ActionButtonModule} from '../component/action-button/action-button.module';
import {InputFormFieldModule} from '../component/input-form-field/input-form-field.module';
import {SelectFormFieldModule} from '../component/select-form-field/select-form-field.module';
import {DateFormFieldModule} from '../component/date-form-field/date-form-field.module';
import {AutocompleteFormFieldModule} from '../component/autocomplete-form-field/autocomplete-form-field.module';
import {TextFilterModule} from '../component/text-filter/text-filter.module';
import {PortFormFieldModule} from '../component/port-form-field/port-form-field.module';

const compoModules = [
  SelectFilterModule,
  TextFilterModule,
  ActionButtonModule,
  InputFormFieldModule,
  SelectFormFieldModule,
  DateFormFieldModule,
  AutocompleteFormFieldModule,
  PortFormFieldModule
];

@NgModule({
  imports: [...compoModules],
  exports: [...compoModules]
})
export class ComponentModule {

}
