import {AbstractControl} from '@angular/forms';

export class PasswordValidation {

  static MatchPassword(control: AbstractControl) {
    const password = control.get('new_password').value;
    const confirmPassword = control.get('confirm_password').value;
    if (password !== confirmPassword) {
      control.get('confirm_password').setErrors({MatchPassword: true})
    } else {
      return null;
    }
  }
}
