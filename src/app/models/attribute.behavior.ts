export class AttributeBehavior {
  private _attributes;

  get attributes() {
    return this._attributes;
  }

  set attributes(attributes) {
    this._attributes = [];
    let key;
    for (key of attributes) {
      if (typeof this[key] === typeof attributes[key]) {
        this[key] = attributes[key];
        this._attributes[key] = attributes[key];
      }
    }
  }
}
